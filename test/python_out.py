#!/usr/bin/env python3
#from multiprocessing import Pool
import MySQLdb
from kafka import KafkaConsumer

#db = MySQLdb.connect(host="172.17.0.2", user="root", passwd="root", db="testdb", charset='utf8')
#cursor = db.cursor()

##############Create table
#sqldb = """CREATE TABLE test (time VARCHAR(100), input VARCHAR(100));"""

##############Clear table
#sqldb = """DELETE FROM test;"""

#cursor.execute(sqldb)
#db.commit()
#db.close()



consumer = KafkaConsumer('test', group_id='my_favorite_group', bootstrap_servers='10.142.0.2:9092,10.142.0.3:9092,10.142.0.4:9092', client_id='python3')



def dbmysql(time_msg, var_msg):
    db = MySQLdb.connect(host="172.17.0.2", user="root", passwd="root", db="testdb", charset='utf8')
    cursor = db.cursor()
    sql = """INSERT INTO test (time,input) VALUES ("%s", "%s");""" % ((str(time_msg), str(var_msg)))
    cursor.execute(sql)
    db.commit() 
    db.close()


#def print_msg(msg):
for msg in consumer:
    print(msg)
    a = str(msg).split(', ')
    time_msg = a[3]
    var_msg = a[6]
    time_msg = str(time_msg).split('=')
    var_msg = str(var_msg).split('=')
    time_msg = time_msg[1]
    var_msg = var_msg[1]
    dbmysql(time_msg, var_msg)
#    print(time_msg)
#    print(var_msg)



#pool = Pool(3)
#pool.map(print_msg, consumer)
